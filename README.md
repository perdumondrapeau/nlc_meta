# Nlc Meta Library

## Introduction

Nlc Meta is a library for compile time C++17 template metaprogramming. It aims to be simple
to use, concise and stl independant. It provides type safe ids and automatic
dimensional analysis too.

The library is divided ten parts :
- [tags.hpp](#tags), basic type tags
- [type_value.hpp](#type_value), type and values encapsulation with comparisons
- [basics.hpp](#basics), substitutes to std::move, std::forward and std::is_same
- [list.hpp and list/*.hpp](#list), type lists and algorithmes on them
- [traits.hpp](#traits), type traits
- [enable_if.hpp](#enable_if), substitute to std::enable_if
- [logic.hpp](#logic), lazy conjonction, disjonction, negation and conditions
- [overloaded.hpp](#overloaded), utilitary for lambda overloading
- [id.hpp](#id), strongly typed ids
- [units.hpp](#units), dimensional analysis

## Documentation

### tags

Nlc Meta provides three tag types for special case representation.
 - `nlc::meta::default_type` can be used as a template parameter as explicit and implementation independant default type.
 - `nlc::meta::Nothing` can be used for explicit representation of absence of type of value.
 - `nlc::meta::error_type` can be used as return type for error cases.

### type_value

**comptime_int**

For convenience, the library provides an alias `nlc::meta::comptime_int` for the compile time integral values.
```Cpp
namespace nlc::meta {
  using comptime_int = long long;
}
```

**Type<T> and type<T>**

``` Cpp
namespace nlc::meta {
  template <typename T> struct Type;
  template <typename T> constexpr Type<T> type;
}
```
`nlc::meta::Type<T>` is a type encapsulation wich can be instantiated in order to pass types as arguments of lamdas at compilation.
`nlc::meta::type<T>` is a value of type `nlc::meta::Type<T>` wich can be used for type comparison with `==` and `!=` operators.

**Value<T> and value<T>**
```Cpp
namespace nlc::meta {
  template <comptime_int v> struct Value;
  template <comptime_int v> constexpr Value<v> value;
}
```
`nlc::meta::Value<T>` is a type encapsulating a compile time integral value, for exemple for usage in typelist, or as result of meta-function.
`nlc::meta::Value<T>` can be used for quick meta-function defintion as in this example :
``` Cpp
using namespace nlc::meta
using l = list<char, int, void, float, void>;
template <typename T> using is_void = Value<is_same<T, void>>;
static_assert(is_same<map<is_void, l>, list<Value<false>, Value<false, Value<true>, Value<false>, Value<true>>>);
```

### basics

**is_same**

`nlc::meta::is_same<T, Q>` is true if T and Q is the same type, false otherwise, as expected.
You can use it with more than two parameters, and it will be true if all the parameters are the same.

**identity**

`nlc::meta::identity` is a meta-function provided for convenience, wich return the type given as a parameter.

**forward and move**

`nlc::forward` and `nlc::move` are carbon copies of `std::forward` and `std::move`.

`nlc_fwd(val)` is a macro to use as a shortcut for `nlc::foward<decltype<val>>(val)`.


### list

**notation for meta-function**

We call _meta function_ a template alias or variable, used to transform a template arguments into a type or a value.
For exemple :
``` Cpp
template <typename T> struct void_to_int_impl { using type = T; };
template <> struct void_to_int_impl { using type = int; };

template <typename T> using void_to_int = typename void_to_int_impl<T>::type;
```
`void_to_int` is a meta-function taking a type `T` has a parameter, and returning `int` if `T` was `void`, and `T` otherwise.

Please note this meta-function can be rewriten using Nlc Meta :
``` Cpp
using namespace nlc::meta;
template <typename T> using void_to_int = if_t<is_same<T, void>, int, T>;
```

**list type and list size**

In order to apply compile time threatement on list of types and values, with use a type list.
``` Cpp
namespace nlc::meta {
  template <typename...T> struct list;
}
```
Values need to be encapsuled in a type, using `nlc::Value`.
``` Cpp
using my_type_list = nlc::meta::list<int, Value<3>, float, Value<4>, Value<false>>;
```

When it is needed to manually initialize a list of compile time integrals, a shortcut can be used :
``` Cpp
using intList = nlc::meta::integral_list<1, 2, 3, 5, 8, 12, 20>;
// equivalent to list<Value<1>, Value<2>, Value<3>, Value<5>, ...
```

**pair, first and second**

`nlc::meta::pair` is an alias for a list with only two elements. The first one can be accessed with meta-function `first` and the second one with meta-function `second`.
``` Cpp
using namespace nlc::meta;
using my_pair = pair<int, float>;
static_assert(is_same<first<my_pair>, int>);
static_assert(is_same<second<my_pair>, float>);
```

**head and tail**

`head` is a meta-function returning the first element of a list, while `tail` return the list without the first element.
``` Cpp
using nlc::meta;
using my_list = integral_list<2, 4, 6, 8>;
static_assert(is_same<head<my_list>, Value<2>>);
static_assert(is_same<tail<my_list>, integral_list<4, 6, 8>>);
```

**meta-functions on lists**

| Name                  |       Type                      | Description |
| :-----------------    | :-----------------------------: | :-----------|
| contains              | L, T -> bool                    | true if L contains at least one occurence of T, false otherwise |
| count                 | T, L -> comptime_int            | Return the number of occurences of T in list L |
| index_of              | T,  -> comptime_int             | Return the index of the first occurence of T in list L. Won't compile if no occurence is present |
| concat                | L... -> L                       | Concatenate all the lists passed as parameter in one list |
| push_back             | L, T -> L                       | Add an element at the end of a list. |
| push_front            | T, L -> L                       | Add an element at the begining of a list. |
| reverse               | L -> L                          | Reverse the order of elements in the list given in argument |
| box                   | T -> T                          | If T is a Value<v> or a Type<T>, let the type unchanged. Otherwise encapsulate it in a Type<T>. |
| unbox                 | T -> T                          | If T is a Value<T>, return T, otherwise let the type unchanged. |
| for_each              |                                 | Take a list as template parameter and a lamda as runtime parameter. Call the lamda with box<T> for each T in the list. |
| apply                 | (T... -> T), L -> T             | Give all the types contained in a list as arguments of a meta-function and return the result |
| remove_doublon        | L -> L                          | Remove all the doublons of a least (only the first occurence of each type is kept) |
| map                   | (T -> T), L -> L                | Apply meta-function F to all the types contained in a list and return the list of the result |
| get                   | i, L -> T                       | Return the type at a given index in a list |
| permutation           | L, L -> T                       | Take a list of indexes (in Value<v>) and a list of types and return the list of the types at the given indexes.|
| select                | (T -> bool), L -> L             | Produce a list of types from a given list for wich a fiven meta-function condition return Value<true>. |
| is_simple_permutation | L, L -> bool                    | Check if each type is present the same number of times in two list (i.e. if you can get one from the others only by shifting the order of elements). |
| intersect             | L, L -> L                       | Construct a list of common elements of two list (warning : intersect<list<int, int, int>, list<int, int>> is list<int, int>). |
| substract             | L, L -> L                       | Remove the elements of the second list from the first list if they are present (warning : substract<list<int, int>> gives list<int> !). |
| range                 | comptime_int, comptime_int -> L | For given arguments a and b with a <= b, produce the list integral_list<a, a + 1, ..., b -1>. |

### traits

**Const, volatile and reference type traits and modifiers**

Nlc Meta provides a few type traits and modifiers :
 - is_const, add_const, rm_const
 - is_ref, add_ref, rm_ref
 - is_volatile, add_volatile, rm_volatile
 - add_const_ref, rm_const_ref
 - add_const_volatile, rm_const_volatile

Lot of type traits are missing in this file and would be added when needed.

| Name                       | Description |
| :------------------------- | :---------- |
| is_integral                | true if argument is one of (possibly cv-qualified) `bool`, `char`, `signed char`, `unsigned char`, `char16_t`, `char32_t`, `wchar_t`, `short`, `unsigned short`, `int`, `unsigned int`, `long`, `unsigned long`, `long long` or `unsigned long long`. |
| if_floating                | true if argument is one of (possibly cv-qualifier) `float`, `double` or `long double`. |
| is_pointer                 | true if argument is a (possible cv-qualified) pointer. |
| is_arithmetic              | true if argument is integral or floating. |
| is_empty_type              |             |
| is_trivially_copyable      |             |
| is_trivially_constructible |             |

### enable_if

`nlc::enable_if` aims to provide the same features than std::enable_if, excepted on the point that the default type returned by nlc::enable_if is int and not void, allowing more concise syntax.

```Cpp
template <typename T, nlc::enable_if<is_int<T>::value> = 0>
auto nlc_enable_if_1(int) { return Value<true> {}; }

template <typename T, typename = nlc::enable_if<is_int<T>::value>>
auto nlc_enable_if_2(int) { return Value<true> {}; }
```

### logic

**Conjonction, disjonction and negation**

`nlc::meta::conjonction`, `nlc::meta::disjonction` are replacement for `&&` and `||`, accepting any number of argument and lazilly evaluating the parameters, offering better compilation time than the common boolean operator in compile time context.
`nlc::meta::negation` is provided for consistency.

**if_t and if_default**

Nlc Meta provides two meta-functions for conditionnal expressions.
The first one `nlc::meta::if_t<Cond, Then, Else>` acts as a `if (cond) Then; else Else;` condition.
The second one `nlc::meta::if_default<Type, Default_type>` return Default_Type if `Type` is `default_type`, and return `Type` otherwise. It is usefull for explicit defaulting of template argument :
```Cpp
template <typename T, typename iterator_t_ = default_type, typename allocator_t_ = default_type> struct Structure {
  using iterator_t_ = if_default<iterator_t_, simple_iterator>;
  using allocator_t = if_default<allocator_t_, global_allocator>;
  ...
}

template <typename T> using SpeciallizedStructure = Structure<T, default_type, specific_allocator>;
```
### overloaded

`nlc::meta::overloaded` is a callable usefull for some kind of overloading of lamda function.
```Cpp
auto f = nlc::meta::overloaded { [] (int v) { cout << "int : " << v << std::endl; },
                                 [] (float v) { cout << "float : " << v << std::endl; } };
f(3); // display "int : 3"
f(4.f); // display "float : 4"
```

### id

Nlc meta provides a class for strongly typed ids. The features are :
 - No implicit conversion
 - Facultative default value
 - Facultative invalid value
 - Facultative increment/decrement

In order to create a new id type, you need to define a descriptor type with specific fields :
```Cpp
struct MyIdDescriptor {
  using underlying_t = int; // required, define how the id value will be stored
  static constexpr underlying_t default_value = 0; // facultative
  static constexpr underlying_t invalid_value = 0; // facultative
  static constexpr bool has_increment = true; // facultative, considered as false if absent.
};
```
 - If no `default_value` is provided, the id won't be instanciated by default
 - If no `invalid_value` is provided, the `invalid()` static methode won't be usable and the `isValid()` method will always return true
 - If no `has_increment` is provided or if its value is false, it won't be possible to use `id + int`, `id - int`, `id += int`, `id -= int`, `++` and `--` operators on id.

To define your type id, simply use :
``` Cpp
using MyId = nlc::meta::id<MyIdDescriptor>;
```

An id can be extended by inheriting, passing the final id type as second template parameter :
```Cpp
struct MyIdDescriptor {
  ...
};

class MyId final : public nlc::meta::id<MyIdDescriptor, MyId> {
  ...
};
```
This construction allows the potential `invalid()` static method to return an Id with the good type.

### units

**Defining new unit or new dimensions**

It is necessary to understand the difference between defining a new unit and a new dimension. A new dimension must be defined in order to represent a brand new type of units wich can't be expressed from existing units. For exemple, speeds can be expressed by a ratio of a distance per a time and is only a new unit, while "People per seconds" need to define a new dimension "Amount of people" and then a new unit as the ratio of "Amount of people" per "seconds".

Defining a new dimension is simply defining a new empty type :
``` Cpp
struct amountOfPeople_t {};
```
``` Cpp
template <typename T> using amountOfPeople = nlc::meta::unit<pair<list<amountOfPeople_t>, list<>>, T>;
template <typename T> using peoplePerSeconds = nlc::meta::unit<pair<list<amountOfPeople_t>, list<nlc::meta::second_t>>, T>
template <typename T> using kilogramPerPeople = nlc::meta::unit<pair<list<nlc::meta::second_t>, list<amountOfPeople>>, T>
```
or alternatively :
``` Cpp
template <typename T> using newtonPerPeople = decltype(nlc::meta::newtons<float>(0) / amountOfPeople<float>(0));
```
wich is equivalent to :
``` Cpp
template <tyepname T> using newtonPerPeople = nlc::meta::unit<pair<list<kilogram_t, meter_t>, list<second_t, second_t, amountOfPeople_t>>, T>;
```

**Usage**

`nlc::meta::unit` is an helper designed to enforce maintainability with dimensional checks on computation. Using it prevents the code from compiling in case of mistakes producing invalid expression like adding a duration to a distance, substituing a speed to an acceleration, or using number of utf-8 codepoints in string in place of it size in bytes.

``` Cpp
using namespace nlc::meta;

struct HP_t {};
template <typename T> using HP = unit<pair<list<HP_t>, list<>>, T>;
using DamagePerMetersPerSecond = decltype(HP<float>(0) / metersPerSeconds<float>(0));

struct Player {
  meters<float> _position;
  metersPerSeconds<float> _speed;
  HP<float> _life;

  auto update() {
    if (detectCollision()) {
      _life -= _speed / DamagePerMetersPerSecond(10); // ERROR : '/' should be '*' !
      _speed /= 2.f;
    }
    _position += _speed; // ERROR : _speed must be multiplied by the tick duration !
  }
};
```

**Prefedined units**

| Unit                | Physical phenomenum | Full type                                                          |
| :------------------ | :------------------ | :----------------------------------------------------------------- |
| meters<T>           | Distance            | unit<pair<list<meter_t, T>, list<>>>                               |
| seconds<T>          | Time                | unit<pair<list<second_t>, list<>>, T>                              |
| kilograms<T>        | Mass                | unit<pair<list<kilogram_t>, list<>>, T>                            |
| amperes<T>          | Electric current    | unit<pair<list<ampere_t>, list<>>, T>                              |
| kelvins<T>          | Temperature         | unit<pair<list<kelvin_t>, list<>>, T>                              |
| moles<T>            | Amount of substance | unit<pair<list<mole_t>, list<>>, T>                                |
| candelas<T>         | Light               | unit<pair<list<candela_t>, list<>>, T>                             |
| pixels<T>           |                     | unit<pair<list<>, list<>>, T>                                      |
| bytes<T>            | Amount of data      | unit<pair<list<>, list<>>, T>                                      |
| metersPerSeconds<T> | Speed               | unit<pair<list<meter_t>, list<second_t>>, T>                       |
| hertz<T>            | Frequency           | unit<pair<list<>, list<second_t>>, T>                              |
| newtons<T>          | Forces              | unit<pair<list<kilogram_t, meter_t>, list<second_t, second_t>>, T> |

**Underlying type cast**

It is possible to change the underlying type of an unit using `nlc::unit_cast`.

``` Cpp
using namespace nlc::meta;

meters<float> fdist {0.f};
meters<int> idist = nlc::unit_cast<int>(fdist);
```
