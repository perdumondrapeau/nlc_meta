/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

namespace nlc::meta {

// identity //////////////////////////////////////////////////////////////////

template<typename T> using identity = T;

}  // namespace nlc::meta

namespace nlc {

namespace meta_impl {  // avoid include over traits.hpp
    template<typename T> struct rm_ref_impl { using type = T; };
    template<typename T> struct rm_ref_impl<T &> : rm_ref_impl<T> {};
    template<typename T> struct rm_ref_impl<T &&> : rm_ref_impl<T> {};
    template<typename T> using rm_ref = typename rm_ref_impl<T>::type;
}  // namespace meta_impl

// forward ///////////////////////////////////////////////////////////////////

template<class T> [[nodiscard]] constexpr T && forward(meta_impl::rm_ref<T> & t) noexcept {
    return static_cast<T &&>(t);
}
template<class T> [[nodiscard]] constexpr T && forward(meta_impl::rm_ref<T> && t) noexcept {
    return static_cast<T &&>(t);
}
#define nlc_fwd(arg) ::nlc::forward<decltype(arg)>(arg)

// move //////////////////////////////////////////////////////////////////////

template<class T> [[nodiscard]] constexpr meta_impl::rm_ref<T> && move(T && t) noexcept {
    return static_cast<meta_impl::rm_ref<T> &&>(t);
}

// declval ///////////////////////////////////////////////////////////////////

template<typename T> extern T declval;

}  // namespace nlc
