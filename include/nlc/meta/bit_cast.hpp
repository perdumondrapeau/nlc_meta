/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

namespace nlc::meta {

namespace impl {
    template<unsigned long size> struct matching_uint_type final {};

    template<> struct matching_uint_type<1> final { using type = unsigned char; };
    template<> struct matching_uint_type<2> final { using type = unsigned short; };
    template<> struct matching_uint_type<4> final { using type = unsigned int; };
    template<> struct matching_uint_type<8> final { using type = unsigned long long; };
}  // namespace impl

// This will cast the input object into an unsigned integer of the same size.
// It can be used to perform bitwise.
template<typename T>
auto bit_cast(T & value) -> typename impl::matching_uint_type<sizeof(T)>::type {
#if defined(__GNUC__) || defined(__clang__)
#    pragma GCC diagnostic push
#    if defined(__clang__)
#        pragma GCC diagnostic ignored "-Wundefined-reinterpret-cast"  //  reinterpret_cast from
                                                                       //  'float' to 'unsigned long
                                                                       //  &' has undefined behavior
#    elif defined(__GNUC__)
#        pragma GCC diagnostic ignored "-Wstrict-aliasing"  // dereferencing type-punned pointer
                                                            // will break strict-aliasing rules
#    endif
#endif

    using int_type = typename impl::matching_uint_type<sizeof(T)>::type;
    static_assert(sizeof(int_type) == sizeof(T));

    return *reinterpret_cast<int_type const *>(&value);

#if defined(__GNUC__) || defined(__clang__)
#    pragma GCC diagnostic pop
#endif
}

}  // namespace nlc::meta
