/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include "tags.hpp"

namespace nlc::meta {

/** enable_if is a concise helper for disabling functions with SFINAE, similarly
 * to what std::enable_if does.
 * usage :
 * template <typename T, meta::enable_if<meta::is_integral<T>> = 0> auto foo(T &&...args) {...}
 */
namespace impl {
    template<bool B, class T = int> struct enable_if_impl {};
    template<typename T> struct enable_if_impl<true, T> { using type = T; };
}  // namespace impl
template<bool B, class T = int> using enable_if = typename impl::enable_if_impl<B, T>::type;

}  // namespace nlc::meta
