/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include "logic.hpp"
#include "traits.hpp"

namespace nlc::meta {

//-------------------------------------------------------------------------

namespace impl {

    //-------------------------------------------------------------------------

#define DEFINE_ACCESSOR_HELPERS(value_name)                                                            \
    template<typename Description>                                                                   \
    constexpr auto is_##value_name##_defined_impl(int)->decltype(Description::value_name, bool {}) { \
        return true;                                                                                   \
    }                                                                                                  \
    template<typename Description> constexpr auto is_##value_name##_defined_impl(float)->bool {      \
        return false;                                                                                  \
    }                                                                                                  \
    template<typename Description> constexpr auto is_##value_name##_defined() {                      \
        return is_##value_name##_defined_impl<Description>(0);                                       \
    }                                                                                                  \
    template<typename Description> constexpr auto get_##value_name() {                               \
        if constexpr (is_##value_name##_defined<Description>())                                      \
            return Description::value_name;                                                          \
        else                                                                                           \
            return error_;                                                                             \
    }

    DEFINE_ACCESSOR_HELPERS(default_value)
    DEFINE_ACCESSOR_HELPERS(invalid_value)
    DEFINE_ACCESSOR_HELPERS(is_copyable)
    DEFINE_ACCESSOR_HELPERS(has_increment)

#undef DEFINE_ACCESSOR_HELPERS

    //-------------------------------------------------------------------------

}  // namespace impl

//-------------------------------------------------------------------------

/* struct Description_t final {
 *     using underlying_type = uint;
 *     static constexpr underlying_type invalid_value = 0;
 *     static constexpr underlying_type default_value = invalid_value;
 *     // static constexpr bool has_increment = true; // false by default
 * }
 */

template<typename Descriptor, typename Self = default_type> class id {
  public:
    using self_type       = if_default<Self, id>;
    using descriptor_type = Descriptor;
    using underlying_type = typename descriptor_type::underlying_type;

    static constexpr auto has_default_value = impl::is_default_value_defined<descriptor_type>();
    static constexpr auto has_invalid_value = impl::is_invalid_value_defined<descriptor_type>();

    [[nodiscard]] static constexpr auto default_value() {
        if constexpr (!has_default_value)
            return error_;
        else
            return impl::get_default_value<descriptor_type>();
    }

    [[nodiscard]] static constexpr auto invalid_value() {
        if constexpr (!has_invalid_value)
            return error_;
        else
            return impl::get_invalid_value<descriptor_type>();
    }

  public:
    constexpr explicit id(underlying_type const & value)
          : _value(value) {}

    constexpr id()
          : _value(default_value()) {}

    constexpr id(id const & rhs) = default;
    constexpr auto operator=(id const & rhs) -> id & = default;

  public:
    [[nodiscard]] constexpr decltype(auto) value() const { return (_value); }
    [[nodiscard]] constexpr decltype(auto) value() { return (_value); }

  public:
    static constexpr auto invalid() {
        if constexpr (has_invalid_value)
            return self_type(invalid_value());
        else
            return error_;
    }

    [[nodiscard]] constexpr auto is_valid() const -> bool {
        if constexpr (has_invalid_value)
            return _value != invalid_value();
        else
            return true;
    }
    [[nodiscard]] constexpr auto is_invalid() const -> bool {
        if constexpr (has_invalid_value)
            return _value == invalid_value();
        else
            return false;
    }

  private:
    underlying_type _value;
};

//-------------------------------------------------------------------------

#define OP(op)                                                                             \
    template<typename D, typename T>                                                       \
    [[nodiscard]] constexpr auto operator op(id<D, T> const & lhs, id<D, T> const & rhs) { \
        return lhs.value() op rhs.value();                                                 \
    }

OP(==)
OP(!=)
OP(<)
OP(>)
OP(<=)
OP(>=)

#undef OP

//-------------------------------------------------------------------------

#define OP(op)                                                                    \
    template<typename D, typename T, typename Integer>                            \
        requires(impl::get_has_increment<D>() && is_integral<Integer>)            \
    [[nodiscard]] constexpr auto operator op(id<D, T> const & lhs, Integer rhs) { \
        return id<D, T>(lhs.value() op rhs);                                      \
    }                                                                             \
    template<typename D, typename T, typename Integer>                            \
        requires(impl::get_has_increment<D>() && is_integral<Integer>)            \
    [[nodiscard]] constexpr auto operator op(Integer lhs, id<D, T> const & rhs) { \
        return id<D, T>(lhs op rhs.value());                                      \
    }                                                                             \
    template<typename D, typename T>                                              \
        requires(impl::get_has_increment<D>())                                    \
    constexpr auto operator op##op(id<D, T> & v)->id<D, T> {                      \
        const auto res = v;                                                       \
        v.value() op##op;                                                         \
        return res;                                                               \
    }                                                                             \
    template<typename D, typename T>                                              \
        requires(impl::get_has_increment<D>())                                    \
    constexpr auto operator op##op(id<D, T> & v, int)->id<D, T> & {               \
        op##op v.value();                                                         \
        return v;                                                                 \
    }                                                                             \
    template<typename D, typename T, typename Integer>                            \
        requires(impl::get_has_increment<D>())                                    \
    inline auto operator op##=(id<D, T> & lhs, Integer rhs)->id<D, T> & {         \
        lhs.value() op## = rhs;                                                   \
        return lhs;                                                               \
    }

OP(+)
OP(-)

#undef OP

//-------------------------------------------------------------------------

}  // namespace nlc::meta
