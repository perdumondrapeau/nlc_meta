/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

namespace nlc::meta {

namespace impl {
    template<typename... T> struct is_same_impl {};
    template<> struct is_same_impl<> { static constexpr auto value = true; };
    template<typename T> struct is_same_impl<T> { static constexpr auto value = true; };
    template<typename T1, typename T2> struct is_same_impl<T1, T2> {
        static constexpr auto value = false;
    };
    template<typename T> struct is_same_impl<T, T> { static constexpr auto value = true; };
    template<typename F, typename S, typename... Next> struct is_same_impl<F, S, Next...> {
        static constexpr auto value = is_same_impl<F, S>::value && is_same_impl<S, Next...>::value;
    };
}  // namespace impl
template<typename... T> inline constexpr auto is_same = impl::is_same_impl<T...>::value;

}  // namespace nlc::meta
