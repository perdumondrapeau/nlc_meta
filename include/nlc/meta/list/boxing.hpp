/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include "../type_value.hpp"

namespace nlc::meta {

namespace impl {
    template<typename T> struct box_impl { using type = Type<T>; };
    template<typename T> struct box_impl<Type<T>> { using type = Type<T>; };
    template<comptime_int v> struct box_impl<Value<v>> { using type = Value<v>; };
    template<typename T> struct unbox_impl { using type = T; };
    template<typename T> struct unbox_impl<Type<T>> { using type = T; };
}  // namespace impl
template<typename T> using box   = typename impl::box_impl<T>::type;
template<typename T> using unbox = typename impl::unbox_impl<T>::type;

}  // namespace nlc::meta
