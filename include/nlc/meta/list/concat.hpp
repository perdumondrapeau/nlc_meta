/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include "../list.hpp"

namespace nlc::meta {

namespace impl {
    template<typename... Ls> struct concat_impl;
    template<> struct concat_impl<> { using type = list<>; };
    template<typename... Ts> struct concat_impl<list<Ts...>> { using type = list<Ts...>; };
    template<typename... Ts1, typename... Ts2> struct concat_impl<list<Ts1...>, list<Ts2...>> {
        using type = list<Ts1..., Ts2...>;
    };
    template<typename L1, typename L2, typename... R> struct concat_impl<L1, L2, R...> {
        using type = typename concat_impl<typename concat_impl<L1, L2>::type, R...>::type;
    };
}  // namespace impl
template<typename... Ls> using concat = typename impl::concat_impl<Ls...>::type;

}  // namespace nlc::meta
