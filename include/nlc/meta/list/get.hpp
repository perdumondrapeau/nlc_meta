/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include "../list.hpp"
#include "../logic.hpp"

namespace nlc::meta {

namespace impl {
    template<comptime_int i, typename L> struct get_impl;
    template<comptime_int i> struct get_impl<i, list<>> { using type = error_type; };
    template<comptime_int i, typename H, typename... T> struct get_impl<i, list<H, T...>> {
        using type = if_t<i == 0, H, typename get_impl<i - 1, list<T...>>::type>;
    };
}  // namespace impl
template<comptime_int i, typename L> using get = typename impl::get_impl<i, L>::type;

}  // namespace nlc::meta
