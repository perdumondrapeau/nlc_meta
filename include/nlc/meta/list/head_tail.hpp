/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include "../list.hpp"

namespace nlc::meta {

namespace impl {
    template<typename... T> struct head_impl;
    template<typename H, typename... T> struct head_impl<list<H, T...>> { using type = H; };
    template<typename... T> struct tail_impl;
    template<typename H, typename... T> struct tail_impl<list<H, T...>> {
        using type = list<T...>;
    };
}  // namespace impl
template<typename T> using head = typename impl::head_impl<T>::type;
template<typename T> using tail = typename impl::tail_impl<T>::type;

}  // namespace nlc::meta
