/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include "../list.hpp"

namespace nlc::meta {

namespace impl {
    template<typename T, typename L> struct index_of_impl;
    template<typename T, typename F, typename... R> struct index_of_impl<T, list<F, R...>> {
        static constexpr auto value =
              static_cast<comptime_int>(1 + index_of_impl<T, list<R...>>::value);
    };
    template<typename T, typename... R> struct index_of_impl<T, list<T, R...>> {
        static constexpr auto value = static_cast<comptime_int>(0);
    };
}  // namespace impl
template<typename T, typename L> inline constexpr auto index_of = impl::index_of_impl<T, L>::value;

}  // namespace nlc::meta
