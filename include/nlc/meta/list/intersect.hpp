/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include "../list.hpp"
#include "../logic.hpp"

#include "count.hpp"
#include "push.hpp"

namespace nlc::meta {

namespace impl {
    template<typename L1, typename L2, typename Acc> struct intersect_impl;
    template<typename L2, typename Acc> struct intersect_impl<list<>, L2, Acc> {
        using type = Acc;
    };
    template<typename H, typename... T, typename L2, typename Acc>
    struct intersect_impl<list<H, T...>, L2, Acc> {
        using next_acc = if_t<is_greater<count<H, L2>, count<H, Acc>>, push_back<Acc, H>, Acc>;
        using type     = typename intersect_impl<list<T...>, L2, next_acc>::type;
    };
}  // namespace impl
template<typename L1, typename L2>
using intersect = typename impl::intersect_impl<L1, L2, list<>>::type;

}  // namespace nlc::meta
