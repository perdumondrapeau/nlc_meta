/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include "../list.hpp"

namespace nlc::meta {

template<typename T1, typename T2> using pair = list<T1, T2>;

namespace impl {
    template<typename T> struct first_impl;
    template<typename T1, typename T2> struct first_impl<pair<T1, T2>> { using type = T1; };
    template<typename T> struct second_impl;
    template<typename T1, typename T2> struct second_impl<pair<T1, T2>> { using type = T2; };
}  // namespace impl
template<typename T> using first  = typename impl::first_impl<T>::type;
template<typename T> using second = typename impl::second_impl<T>::type;

}  // namespace nlc::meta
