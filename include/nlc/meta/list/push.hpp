/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include "../list.hpp"

namespace nlc::meta {

namespace impl {
    template<typename T, typename L> struct push_back_impl;
    template<typename T, typename... Ts> struct push_back_impl<T, list<Ts...>> {
        using type = list<Ts..., T>;
    };
    template<typename T, typename L> struct push_front_impl;
    template<typename T, typename... Ts> struct push_front_impl<T, list<Ts...>> {
        using type = list<T, Ts...>;
    };
}  // namespace impl
template<typename L, typename T> using push_back  = typename impl::push_back_impl<T, L>::type;
template<typename T, typename L> using push_front = typename impl::push_front_impl<T, L>::type;

}  // namespace nlc::meta
