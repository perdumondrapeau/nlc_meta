/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include "../list.hpp"
#include "../type_value.hpp"

#include "push.hpp"

namespace nlc::meta {

namespace impl {
    template<long long int b, long long int e> struct range_impl {
        static_assert(b < e);
        using type = push_front<Value<b>, typename range_impl<b + 1, e>::type>;
    };
    template<long long int b> struct range_impl<b, b> { using type = list<>; };
}  // namespace impl
template<long long int b, long long int e> using range = typename impl::range_impl<b, e>::type;

}  // namespace nlc::meta
