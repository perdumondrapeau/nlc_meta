/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include "../list.hpp"
#include "../logic.hpp"

#include "contains.hpp"
#include "push.hpp"

namespace nlc::meta {

namespace impl {
    template<typename L, typename Acc> struct remove_doublons_impl;
    template<typename Acc> struct remove_doublons_impl<list<>, Acc> { using type = Acc; };
    template<typename H, typename... T, typename Acc>
    struct remove_doublons_impl<list<H, T...>, Acc> {
        using type =
              typename remove_doublons_impl<list<T...>, if_t<contains<Acc, H>, Acc, push_back<Acc, H>>>::type;
    };
}  // namespace impl
template<typename L> using remove_doublons = typename impl::remove_doublons_impl<L, list<>>::type;

}  // namespace nlc::meta
