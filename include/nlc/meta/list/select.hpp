/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include "../list.hpp"
#include "../logic.hpp"

#include "push.hpp"

namespace nlc::meta {

namespace impl {
    template<template<typename> typename C, typename L, typename Acc> struct select_impl;
    template<template<typename> typename C, typename Acc> struct select_impl<C, list<>, Acc> {
        using type = Acc;
    };
    template<template<typename> typename C, typename H, typename... T, typename Acc>
    struct select_impl<C, list<H, T...>, Acc> {
        using next_acc = if_t<C<H>::value, push_back<Acc, H>, Acc>;
        using type     = typename select_impl<C, list<T...>, next_acc>::type;
    };
}  // namespace impl
template<template<typename> typename C, typename L>
using select = typename impl::select_impl<C, L, list<>>::type;

}  // namespace nlc::meta
