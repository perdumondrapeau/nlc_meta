/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include "../list.hpp"
#include "../logic.hpp"

#include "count.hpp"
#include "push.hpp"

namespace nlc::meta {

namespace impl {
    template<typename L1, typename L2, typename Res, typename Acc> struct substract_impl;
    template<typename L2, typename Res, typename Acc> struct substract_impl<list<>, L2, Res, Acc> {
        using type = Res;
    };
    template<typename H, typename... T, typename L2, typename Res, typename Acc>
    struct substract_impl<list<H, T...>, L2, Res, Acc> {
        using type = if_t<is_greater<count<H, L2>, count<H, Acc>>,
                          typename substract_impl<list<T...>, L2, Res, push_back<Acc, H>>::type,
                          typename substract_impl<list<T...>, L2, push_back<Res, H>, Acc>::type>;
    };
}  // namespace impl
template<typename L1, typename L2>
using substract = typename impl::substract_impl<L1, L2, list<>, list<>>::type;

}  // namespace nlc::meta
