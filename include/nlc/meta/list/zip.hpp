/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include "../list.hpp"
#include "../logic.hpp"
#include "../tags.hpp"

#include "get.hpp"
#include "head_tail.hpp"
#include "map.hpp"

namespace nlc::meta {

namespace impl {
    template<typename... Ls> struct zip_impl {
        template<typename i> using multi_get  = list<get<i::value, Ls>...>;
        static constexpr auto first_list_size = head<list<Ls...>>::size;
        static constexpr bool is_input_valid  = ((Ls::size == first_list_size) && ...);
        using type = if_t<is_input_valid, map<multi_get, range<0, first_list_size>>, error_type>;
    };
}  // namespace impl
template<typename... Ls> using zip = typename impl::zip_impl<Ls...>::type;

}  // namespace nlc::meta
