/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include "basics.hpp"
#include "tags.hpp"

namespace nlc::meta {

// conjonction ///////////////////////////////////////////////////////////////

namespace impl {
    template<auto... vs> struct conjonction_impl {};
    template<> struct conjonction_impl<> { static constexpr auto value = true; };
    template<> struct conjonction_impl<true> { static constexpr auto value = true; };
    template<> struct conjonction_impl<false> { static constexpr auto value = false; };
    template<auto... vs> struct conjonction_impl<true, vs...> {
        static constexpr auto value = conjonction_impl<vs...>::value;
    };
    template<auto... vs> struct conjonction_impl<false, vs...> {
        static constexpr auto value = false;
    };
}  // namespace impl
template<auto... vs> inline constexpr auto conjonction = impl::conjonction_impl<vs...>::value;

// disjonction ///////////////////////////////////////////////////////////////

namespace impl {
    template<auto... vs> struct disjonction_impl {};
    template<> struct disjonction_impl<> { static constexpr auto value = false; };
    template<> struct disjonction_impl<true> { static constexpr auto value = true; };
    template<> struct disjonction_impl<false> { static constexpr auto value = false; };
    template<auto... vs> struct disjonction_impl<true, vs...> {
        static constexpr auto value = true;
    };
    template<auto... vs> struct disjonction_impl<false, vs...> {
        static constexpr auto value = disjonction_impl<vs...>::value;
    };
}  // namespace impl
template<auto... vs> inline constexpr auto disjonction = impl::disjonction_impl<vs...>::value;

// negation //////////////////////////////////////////////////////////////////

template<bool b> inline constexpr auto negation = !b;

// if_t //////////////////////////////////////////////////////////////////////

namespace impl {
    template<bool b> struct if_t_impl { template<typename T, typename Q> using type = T; };
    template<> struct if_t_impl<false> { template<typename T, typename Q> using type = Q; };

    template<typename> struct if_default_impl { template<typename T, typename> using type = T; };
    template<> struct if_default_impl<default_type> { template<typename, typename Q> using type = Q; };
}  // namespace impl
template<bool B, typename T, typename Q>
using if_t = typename impl::if_t_impl<B>::template type<T, Q>;

template<typename T, typename Q>
using if_default = typename impl::if_default_impl<T>::template type<T, Q>;

}  // namespace nlc::meta
