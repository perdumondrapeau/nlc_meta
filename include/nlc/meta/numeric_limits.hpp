/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include "bit_cast.hpp"

namespace nlc::meta {

template<typename T> struct limits;

namespace impl {
    constexpr auto is_char_signed = static_cast<char>(-1) < 0;

    template<bool is_char_signed = true> struct char_limits_impl {
        static constexpr char min = static_cast<char>(-128);
        static constexpr char max = static_cast<char>(127);
    };

    template<> struct char_limits_impl<false> {
        static constexpr char min = static_cast<char>(0u);
        static constexpr char max = static_cast<char>(255u);
    };

    using char_limits = char_limits_impl<is_char_signed>;
}  // namespace impl

template<> struct limits<char> : public impl::char_limits {};

template<> struct limits<signed char> {
    static constexpr signed char min = -128;
    static constexpr signed char max = 127;
};

template<> struct limits<unsigned char> {
    static constexpr unsigned char min = 0u;
    static constexpr unsigned char max = 255u;
};

template<> struct limits<short int> {
    static constexpr short int min = -32768;
    static constexpr short int max = 32767;
};

template<> struct limits<unsigned short int> {
    static constexpr unsigned short int min = 0u;
    static constexpr unsigned short int max = 65535u;
};

template<> struct limits<int> {
    static constexpr int min = -2147483647 - 1;
    static constexpr int max = 2147483647;
};

template<> struct limits<unsigned int> {
    static constexpr unsigned int min = 0u;
    static constexpr unsigned int max = 4294967295u;
};

template<> struct limits<long long int> {
    static constexpr long long int min = -9223372036854775807ll - 1ll;
    static constexpr long long int max = 9223372036854775807ll;
};

template<> struct limits<unsigned long long int> {
    static constexpr unsigned long long int min = 0ull;
    static constexpr unsigned long long int max = 18446744073709551615ull;
};

template<> struct limits<long int> {
    static constexpr long int min = static_cast<long int>(sizeof(long int) == sizeof(int)  //
                                                                ? limits<int>::min
                                                                : limits<long long int>::min);
    static constexpr long int max = static_cast<long int>(sizeof(long int) == sizeof(int)  //
                                                                ? limits<int>::max
                                                                : limits<long long int>::max);
};

template<> struct limits<unsigned long int> {
    static constexpr unsigned long int min =
          static_cast<unsigned long int>(sizeof(long int) == sizeof(int)  //
                                               ? limits<unsigned int>::min
                                               : limits<unsigned long long int>::min);
    static constexpr unsigned long int max =
          static_cast<unsigned long>(sizeof(long int) == sizeof(int)  //
                                           ? limits<unsigned int>::max
                                           : limits<unsigned long long int>::max);
};

template<> struct limits<float> final {
    static constexpr float min     = -3.402823466e+38F;
    static constexpr float max     = 3.402823466e+38F;
    static constexpr float epsilon = 1.192092896e-07F;  // smallest such that 1.0+epsilon != 1.0
#if defined(_MSC_VER)
    static constexpr float infinity = __builtin_huge_valf();
#else
    static constexpr float infinity = __builtin_inff();
#endif

    constexpr static bool is_finite(float const v) noexcept {
#if defined(__GNUC__) || defined(__clang__)
        return !__builtin_isinf(v);
#else
        return v != infinity && v != -infinity;
#endif
    }

    static bool is_nan(float const v) noexcept {
#if defined(__GNUC__) || defined(__clang__)
        return __builtin_isnan(v);
#else
        constexpr auto exponent_mask = 0b0'11111111'00000000000000000000000u;
        constexpr auto fraction_mask = 0b0'00000000'11111111111111111111111u;

        auto const bits = bit_cast(v);
        return (bits & exponent_mask) == exponent_mask && (bits & fraction_mask) != 0;
#endif
    }
};

template<> struct limits<double> {
    static constexpr double min     = -1.7976931348623158e+308;
    static constexpr double max     = 1.7976931348623158e+308;
    static constexpr double epsilon = 2.2204460492503131e-016;  // smallest such that 1.0+epsilon
                                                                // != 1.0
#if defined(_MSC_VER)
    static constexpr double infinity = __builtin_huge_val();
#else
    static constexpr double infinity = __builtin_inf();
#endif

    constexpr static bool is_finite(double const v) noexcept {
#if defined(__GNUC__) || defined(__clang__)
        return !__builtin_isinf(v);
#else
        return v != infinity && v != -infinity;
#endif
    }

    static bool is_nan(double const v) noexcept {
#if defined(__GNUC__) || defined(__clang__)
        return __builtin_isnan(v);
#else
        constexpr auto exponent_mask =
              0b0'11111111111'0000000000000000000000000000000000000000000000000000u;
        constexpr auto fraction_mask =
              0b0'00000000000'1111111111111111111111111111111111111111111111111111u;

        auto const bits = bit_cast(v);
        return (bits & exponent_mask) == exponent_mask && (bits & fraction_mask) != 0;
#endif
    }
};

}  // namespace nlc::meta
