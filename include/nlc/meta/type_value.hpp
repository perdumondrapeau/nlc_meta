/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

namespace nlc::meta {

// -------------------------------------------------

using comptime_int = long long;

// -------------------------------------------------

/**
 * value_t<v> is a wrapper around a compile time value v for metaprograming
 * @tparam v : the value to wrap
 */
template<comptime_int v> struct Value {
    static constexpr auto value = v;

    constexpr operator decltype(v)() const { return v; }
};

/**
 * value_v is a value of type value_t<v>
 * @tparam v
 */
template<comptime_int v> [[maybe_unused]] constexpr Value<v> value {};

#define OP(op)                                                     \
    template<comptime_int v, comptime_int w>                       \
    [[nodiscard]] constexpr auto operator op(Value<v>, Value<w>) { \
        constexpr auto res = v op w;                               \
        return value<res>;                                         \
    }

OP(==)
OP(!=)
OP(<)
OP(>)
OP(<=)
OP(>=)
#undef OP

// Comparisons ///////////////////////////////////////////////////////////////

template<auto a, auto b> inline constexpr auto is_greater = a > b;
template<auto a, auto b> inline constexpr auto is_less    = a < b;
template<auto a, auto b> inline constexpr auto is_equal   = a == b;

// -------------------------------------------------

/**
 * type_t<T> is a wrapper around a type T, on order to manipulate types as
 * values in metaprograming
 * @tparam T : the type to wrap
 */
template<typename T> struct Type final { using type = T; };

/**
 * type_v<V> is a value of type type_t<T> available for syntax simplification in
 * metaprograming
 * @tparam T : the type to wrap
 */
template<typename T> [[maybe_unused]] constexpr Type<T> type {};

template<typename T, typename Q> [[nodiscard]] constexpr bool operator==(Type<T>, Type<Q>) {
    return false;
}
template<typename T, typename Q> [[nodiscard]] constexpr bool operator!=(Type<T>, Type<Q>) {
    return true;
}
template<typename T> [[nodiscard]] constexpr bool operator==(Type<T>, Type<T>) { return true; }
template<typename T> [[nodiscard]] constexpr bool operator!=(Type<T>, Type<T>) { return false; }

// -------------------------------------------------

template<typename T, comptime_int v> [[nodiscard]] constexpr auto operator==(Type<T>, Value<v>) {
    return false;
}
template<typename T, comptime_int v> [[nodiscard]] constexpr auto operator!=(Type<T>, Value<v>) {
    return true;
}
template<typename T, comptime_int v> [[nodiscard]] constexpr auto operator==(Value<v>, Type<T>) {
    return false;
}
template<typename T, comptime_int v> [[nodiscard]] constexpr auto operator!=(Value<v>, Type<T>) {
    return true;
}

}  // namespace nlc::meta
