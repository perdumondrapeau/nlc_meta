/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include "list/concat.hpp"
#include "list/intersect.hpp"
#include "list/pair.hpp"
#include "list/permutation.hpp"
#include "list/substract.hpp"

#include "list.hpp"
#include "logic.hpp"
#include "traits.hpp"

namespace nlc::meta {

// Dimension/////////////////////////////////////////////

namespace impl {

    template<typename Dim1, typename Dim2>
    inline constexpr auto is_equivalent_dim = is_simple_permutation<first<Dim1>, first<Dim2>> &&
                          is_simple_permutation<second<Dim1>, second<Dim2>>;

    // ------------------------------------------

    enum class unit_op { SUM, MULT, DIV };

    // ------------------------------------------

    template<unit_op op, typename Dim1, typename Dim2> struct op_type { using type = error_type; };

    // ------------------------------------------

    template<typename Dim> struct simplify_impl {
        using inter = intersect<first<Dim>, second<Dim>>;
        using type  = pair<substract<first<Dim>, inter>, substract<second<Dim>, inter>>;
    };
    template<typename Dim> using simplify = typename simplify_impl<Dim>::type;

    // ------------------------------------------

    template<typename Dim1, typename Dim2> struct op_type<unit_op::SUM, Dim1, Dim2> {
        using type = if_t<is_equivalent_dim<Dim1, Dim2>, Dim1, error_type>;
    };

    // ------------------------------------------

    template<typename Dim1, typename Dim2> struct op_type<unit_op::MULT, Dim1, Dim2> {
        using type =
              simplify<pair<concat<first<Dim1>, first<Dim2>>, concat<second<Dim1>, second<Dim2>>>>;
    };

    // ------------------------------------------
    template<typename Dim1, typename Dim2> struct op_type<unit_op::DIV, Dim1, Dim2> {
        using type =
              simplify<pair<concat<first<Dim1>, second<Dim2>>, concat<second<Dim1>, first<Dim2>>>>;
    };

    // ------------------------------------------
}  // namespace impl

// unit /////////////////////////////////////////////////

template<typename D, typename T = float> class unit final {
  public:
    using underlying = T;
    using dim        = D;

  public:
    unit()                       = default;
    constexpr unit(unit &&)      = default;
    constexpr unit(unit const &) = default;
    auto operator=(unit &&) -> unit & = default;
    auto operator=(unit const &) -> unit & = default;

    constexpr unit(T const & val)
          : v(val) {}
    auto operator=(T const & val) -> unit & {
        v = val;
        return *this;
    }

    explicit constexpr operator T &() { return v; }
    explicit constexpr operator T const &() const { return v; }

    template<typename E, typename Q>
        requires impl::is_equivalent_dim<D, E>
    explicit constexpr operator unit<E, Q>() const { return unit<E, Q>(static_cast<Q>(v)); }

    template<typename E>
        requires impl::is_equivalent_dim<D, E>
    constexpr operator unit<E, T>() const { return unit<E, T>(v); }

  public:
    T v;
};
template<typename T> unit(T &&) -> unit<rm_const_ref<T>, list<list<>, list<>>>;

// is_unit /////////////////////////////////////////////////////////////

namespace impl {
    template<typename T> struct is_unit_impl : Value<false> {};
    template<typename D, typename T> struct is_unit_impl<unit<D, T>> : Value<true> {};
}  // namespace impl
template<typename T> inline constexpr auto is_unit = impl::is_unit_impl<T>::value;

// Standard operations /////////////////////////////////////////////////

namespace impl {
    template<typename U1, typename U2> struct is_equivalent {
        static constexpr auto value = false;
    };
    template<typename D1, typename D2, typename T> struct is_equivalent<unit<D1, T>, unit<D2, T>> {
        static constexpr auto value = is_equivalent_dim<D1, D2>;
    };
}  // namespace impl
template<typename U1, typename U2>
inline constexpr auto is_equivalent = impl::is_equivalent<U1, U2>::value;

#define define_op(op)                                                                                   \
    template<typename D1, typename D2, typename T>                                                      \
    [[nodiscard]] constexpr inline auto operator op(unit<D1, T> const & lhs, unit<D2, T> const & rhs) { \
        return static_cast<T const &>(lhs) op static_cast<T const &>(rhs);                              \
    }                                                                                                   \
    template<typename D, typename T, typename Q>                                                        \
    [[nodiscard]] constexpr inline auto operator op(unit<D, T> const & lhs, Q const & rhs) {            \
        return static_cast<T const &>(lhs) op rhs;                                                      \
    }                                                                                                   \
    template<typename D, typename T, typename Q>                                                        \
    [[nodiscard]] constexpr inline auto operator op(Q const & lhs, unit<D, T> const & rhs) {            \
        return lhs op static_cast<T const &>(rhs);                                                      \
    }

define_op(==) define_op(!=) define_op(<) define_op(>) define_op(<=) define_op(>=)
#undef define_op

// Standard operations /////////////////////////////////////////////////

#define define_op(op, OP)                                                                               \
    template<typename D1, typename D2, typename T>                                                      \
    [[nodiscard]] constexpr inline auto operator op(unit<D1, T> const & lhs, unit<D2, T> const & rhs) { \
        return unit<typename impl::op_type<impl::unit_op::OP, D1, D2>::type, T>(                              \
              static_cast<T const &>(lhs) op static_cast<T const &>(rhs));                              \
    }

      define_op(+, SUM) define_op(-, SUM) define_op(*, MULT) define_op(/, DIV)
#undef define_op

            template<typename D, typename T, typename Q>
            [[nodiscard]] constexpr inline auto operator*(unit<D, T> const & lhs, Q const & rhs) {
    return unit<D, T>(static_cast<T const &>(lhs) * rhs);
}

template<typename D, typename T, typename Q>
[[nodiscard]] constexpr inline auto operator*(Q const & lhs, unit<D, T> const & rhs) {
    return unit<D, T>(lhs * static_cast<T const &>(rhs));
}

template<typename D, typename T, typename Q>
[[nodiscard]] constexpr inline auto operator/(unit<D, T> const & lhs, Q const & rhs) {
    return unit<D, T>(static_cast<T const &>(lhs) / rhs);
}

template<typename D, typename T, typename Q>
[[nodiscard]] constexpr inline auto operator/(Q const & lhs, unit<D, T> const & rhs) {
    return unit<list<second<D>, first<D>>, T>(lhs / static_cast<T const &>(rhs));
}

template<typename D1, typename D2, typename T>
constexpr inline auto operator+=(unit<D1, T> & lhs, unit<D2, T> const & rhs)
      -> unit<typename impl::op_type<impl::unit_op::SUM, D1, D2>::type, T> & {
    static_cast<T &>(lhs) += static_cast<T const &>(rhs);
    return lhs;
}

template<typename D1, typename D2, typename T>
constexpr inline auto operator-=(unit<D1, T> & lhs, unit<D2, T> const & rhs)
      -> unit<typename impl::op_type<impl::unit_op::SUM, D1, D2>::type, T> & {
    static_cast<T &>(lhs) -= static_cast<T const &>(rhs);
    return lhs;
}

template<typename D, typename T, typename Q>
constexpr inline auto operator*=(unit<D, T> & lhs, Q const & rhs) -> unit<D, T> & {
    static_cast<T &>(lhs) *= rhs;
    return lhs;
}

template<typename D, typename T, typename Q>
constexpr inline auto operator/=(unit<D, T> & lhs, Q const & rhs) -> unit<D, T> & {
    static_cast<T &>(lhs) /= rhs;
    return lhs;
}

template<typename D, typename T>
constexpr inline auto operator-(unit<D, T> const & lhs) -> unit<D, T> {
    return { -lhs.v };
}

// Standard unites /////////////////////////////////////////////////////

#define define_dimensions(name) \
    struct name##_t final {};   \
    template<typename T = float> using name##s = unit<pair<list<name##_t>, list<>>, T>

// ISO units
define_dimensions(meter);
define_dimensions(second);
define_dimensions(kilogram);
define_dimensions(ampere);
define_dimensions(kelvin);
define_dimensions(mole);
define_dimensions(candela);

// Specific unit
define_dimensions(pixel);
define_dimensions(byte);

#undef define_dimensions

// Compound units
template<typename T = float> using meters_per_second = unit<pair<list<meter_t>, list<second_t>>, T>;
template<typename T = float> using hertz           = unit<pair<list<>, list<second_t>>, T>;
template<typename T = float>
using newtons = unit<pair<list<kilogram_t, meter_t>, list<second_t, second_t>>, T>;
}  // namespace nlc::meta

namespace nlc {

template<typename D, typename T>
inline constexpr auto rm_unit(meta::unit<D, T> const & u) -> T const & {
    return u.v;
}
template<typename D, typename T>
inline constexpr auto rm_unit(meta::unit<D, T> & u) -> T & {
    return u.v;
}

// unit_cast ///////////////////////////////////////////////////////////

template<typename NewType, typename D, typename T>
inline constexpr auto unit_cast(meta::unit<D, T> const & u) {
    return meta::unit<D, NewType> { static_cast<NewType>(rm_unit(u)) };
}

}  // namespace nlc
