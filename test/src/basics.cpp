/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/meta/basics.hpp>
#include <nlc/meta/is_same.hpp>
#include <nlc/meta/type_value.hpp>

using namespace nlc::meta;

// identity //////////////////////////////////////////////////////////////////

static_assert(type<identity<int>> == type<int>);

// forward ///////////////////////////////////////////////////////////////////

struct LValue {};
struct RValue {};

[[maybe_unused]] static auto lvalue = 43;

static auto test_valueness(int const &) { return LValue {}; }
static auto test_valueness(int &&) { return RValue {}; }

template<typename T> auto forward_foo(T && arg) { return test_valueness(nlc::forward<T>(arg)); }

static_assert(is_same<decltype(forward_foo(lvalue)), LValue>);
static_assert(is_same<decltype(forward_foo(42)), RValue>);

// move //////////////////////////////////////////////////////////////////////

static_assert(is_same<decltype(test_valueness(lvalue)), LValue>);
static_assert(is_same<decltype(test_valueness(nlc::move(lvalue))), RValue>);

// declval ///////////////////////////////////////////////////////////////////

static_assert(is_same<decltype(nlc::declval<int>), int>);
static_assert(is_same<decltype(nlc::declval<int const>), int const>);
static_assert(is_same<decltype(nlc::declval<int &>), int &>);
static_assert(is_same<decltype(nlc::declval<int &&>), int &&>);
static_assert(is_same<decltype(nlc::declval<int const &>), int const &>);

// main //////////////////////////////////////////////////////////////////////

int main() { return 0; }
