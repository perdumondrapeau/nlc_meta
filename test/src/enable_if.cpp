/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/meta/basics.hpp>
#include <nlc/meta/enable_if.hpp>
#include <nlc/meta/is_same.hpp>
#include <nlc/meta/type_value.hpp>

using namespace nlc;
using namespace nlc::meta;

template<typename T> struct is_int { static constexpr auto value = false; };
template<> struct is_int<int> { static constexpr auto value = true; };

template<typename T, enable_if<is_int<T>::value> = 0> auto test_enable_if_1(int) {
    return Value<true> {};
}
template<typename T> auto test_enable_if_1(float) { return Value<false> {}; }
template<typename T, typename = enable_if<is_int<T>::value>> auto test_enable_if_2(int) {
    return Value<true> {};
}
template<typename T> auto test_enable_if_2(float) { return Value<false> {}; }

static_assert(is_same<decltype(test_enable_if_1<int>(0)), Value<true>>);
static_assert(is_same<decltype(test_enable_if_1<char>(0)), Value<false>>);
static_assert(is_same<decltype(test_enable_if_2<int>(0)), Value<true>>);
static_assert(is_same<decltype(test_enable_if_2<char>(0)), Value<false>>);

auto main() -> int {}
