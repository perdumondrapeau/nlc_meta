/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/meta/id.hpp>
#include <nlc/meta/is_same.hpp>

#include "../utils.hpp"

using namespace nlc::meta;

struct Ad {
    using underlying_type = int;
};
using A = id<Ad>;

struct Bd {
    using underlying_type = int;

    static constexpr auto default_value = 0;
};
using B = id<Bd>;

struct Cd {
    using underlying_type = int;

    static constexpr auto invalid_value = 0;
};
using C = id<Cd>;

struct Dd {
    using underlying_type = int;

    static constexpr auto invalid_value = 0;
    static constexpr auto default_value = 1;
};
struct D : public id<Dd, D> {
    using id<Dd, D>::id;
    auto patate() { return 3; }
};

static_assert(is_same<Ad::underlying_type, int, A::underlying_type>);
static_assert(is_same<Ad, A::descriptor_type>);
static_assert(is_same<A, A::self_type>);
static_assert(is_same<decltype(A::invalid()), error_type>);
// static_assert_fails_to_compile(NO_DEFAULT_CONSTRUCTOR_A, A());

static_assert(is_same<Bd::underlying_type, int, B::underlying_type>);
static_assert(is_same<Bd, B::descriptor_type>);
static_assert(is_same<B, B::self_type>);
static_assert(is_same<decltype(B::invalid()), error_type>);
static auto test_B() { assert(B {}.value() == Bd::default_value); }

static_assert(is_same<Cd::underlying_type, int, C::underlying_type>);
static_assert(is_same<Cd, C::descriptor_type>);
static_assert(is_same<C, C::self_type>);
static_assert(is_same<decltype(C::invalid()), C>);
// static_assert_fails_to_compile(NO_DEFAULT_CONSTRUCTOR_C, C());
static auto test_C() { assert(C::invalid().value() == Cd::invalid_value); }

static_assert(is_same<Dd::underlying_type, int, D::underlying_type>);
static_assert(is_same<Dd, D::descriptor_type>);
static_assert(is_same<D, D::self_type>);
static_assert(is_same<decltype(D::invalid()), D>);
static auto test_D() {
    auto d = D();
    assert(d.value() == Dd::default_value);
    assert(d.is_valid());
    d = D::invalid();
    assert(d.value() == Dd::invalid_value);
    assert(!d.is_valid());
    assert(d.patate() == 3);
}

int main() {
    // test_A();
    test_B();
    test_C();
    test_D();
    return 0;
}
