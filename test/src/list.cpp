/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <string>

#include <nlc/meta/is_same.hpp>
#include <nlc/meta/list.hpp>
#include <nlc/meta/list/apply.hpp>
#include <nlc/meta/list/boxing.hpp>
#include <nlc/meta/list/concat.hpp>
#include <nlc/meta/list/contains.hpp>
#include <nlc/meta/list/count.hpp>
#include <nlc/meta/list/foreach.hpp>
#include <nlc/meta/list/get.hpp>
#include <nlc/meta/list/head_tail.hpp>
#include <nlc/meta/list/index_of.hpp>
#include <nlc/meta/list/intersect.hpp>
#include <nlc/meta/list/map.hpp>
#include <nlc/meta/list/pair.hpp>
#include <nlc/meta/list/permutation.hpp>
#include <nlc/meta/list/push.hpp>
#include <nlc/meta/list/range.hpp>
#include <nlc/meta/list/remove_doublons.hpp>
#include <nlc/meta/list/reverse.hpp>
#include <nlc/meta/list/select.hpp>
#include <nlc/meta/list/substract.hpp>
#include <nlc/meta/list/zip.hpp>
#include <nlc/meta/overloaded.hpp>

#include "../utils.hpp"

namespace nlc::meta {

static_assert(list<>::size == 0);
static_assert(list<int>::size == 1);
static_assert(list<Value<0>, Value<2>, Value<4>(), Value<8>>::size == 4);

static_assert(is_same<first<pair<int, float>>, int>);
static_assert(is_same<second<pair<int, float>>, float>);
static_assert(is_same<pair<int, float>, list<int, float>>);

static_assert(is_same<integral_list<>, list<>>);
static_assert(integral_list<0>::size == 1);
static_assert(is_same<integral_list<0>, list<Value<0>>>);
static_assert(is_same<integral_list<0, 1>, list<Value<0>, Value<1>>>);

static_assert(is_same<head<list<int>>, int>);
static_assert(is_same<tail<list<int>>, list<>>);
static_assert(is_same<head<list<int, float, double>>, int>);
static_assert(is_same<tail<list<int, float, double>>, list<float, double>>);

static_assert(!contains<list<>, int>);
static_assert(!contains<list<float>, int>);
static_assert(!contains<list<float, double, char>, int>);
static_assert(contains<list<int>, int>);
static_assert(contains<list<float, int, double>, int>);
static_assert(contains<list<int, float, double>, int>);
static_assert(contains<list<float, double, int>, int>);

static_assert(count<int, list<>> == 0);
static_assert(count<int, list<float>> == 0);
static_assert(count<int, list<float, double, char, short>> == 0);
static_assert(count<int, list<int>> == 1);
static_assert(count<int, list<int, double, char, short>> == 1);
static_assert(count<int, list<double, float, char, int>> == 1);
static_assert(count<int, list<int, double, int, int, float, int, char>> == 4);
static_assert(count<int, list<int, int, int, int, int>> == 5);

// TODO: make index_of return an invalid value and test this through static_assert
// static_assert_fails_to_compile(index_of<int, list<>>);
static_assert(index_of<int, list<int>> == 0);
static_assert(index_of<int, list<int, float, double>> == 0);
static_assert(index_of<int, list<float, int, double>> == 1);
static_assert(index_of<int, list<float, double, int>> == 2);
static_assert(index_of<int, list<float, int, double, char, int, short>> == 1);

static_assert(is_same<concat<>, list<>>);
static_assert(is_same<concat<list<>>, list<>>);
static_assert(is_same<concat<list<>, list<>>, list<>>);
static_assert(is_same<concat<list<>, list<>, list<>, list<>>, list<>>);
static_assert(is_same<concat<list<int, float>>, list<int, float>>);
static_assert(is_same<concat<list<int, float>, list<double>>, list<int, float, double>>);
static_assert(is_same<concat<list<int, float>, list<char, double>, list<int, short>>,
                      list<int, float, char, double, int, short>>);

static_assert(is_same<push_back<list<>, int>, list<int>>);
static_assert(is_same<push_back<list<double, float>, int>, list<double, float, int>>);
static_assert(is_same<push_front<int, list<>>, list<int>>);
static_assert(is_same<push_front<int, list<double, float>>, list<int, double, float>>);

static_assert(is_same<box<int>, Type<int>>);
static_assert(is_same<box<Type<int>>, Type<int>>);
static_assert(is_same<box<Value<0>>, Value<0>>);
static_assert(is_same<unbox<int>, int>);
static_assert(is_same<unbox<Type<int>>, int>);
static_assert(is_same<unbox<Value<0>>, Value<0>>);

static_assert(is_same<reverse<list<>>, list<>>);
static_assert(is_same<reverse<list<int>>, list<int>>);
static_assert(is_same<reverse<list<int, float>>, list<float, int>>);
static_assert(is_same<reverse<integral_list<3, 4, 5>>, integral_list<5, 4, 3>>);
static_assert(is_same<reverse<integral_list<0, 1, 2, 3, 4, 5, 6>>, integral_list<6, 5, 4, 3, 2, 1, 0>>);

static auto test_for_each_1() -> bool {
    auto temp = std::string {};

    auto add = overloaded { [&](Value<0>) { temp += "0"; },    [&](Value<1>) { temp += "1"; },
                            [&](Value<2>) { temp += "2"; },    [&](Value<3>) { temp += "3"; },
                            [&](Type<int>) { temp += "i"; },   [&](Type<float>) { temp += "f"; },
                            [&](Type<double>) { temp += "d"; } };

    for_each<list<Value<0>, int, Value<3>, float, double>>(add);
    if (temp == "0i3fd")
        return true;
    return false;
}

static auto test_for_each_2() -> bool {
    auto temp = std::string {};
    for_each<list<int, Value<2>, double, Value<1>, float>>([&](auto v) {
        if constexpr (v == type<int>)
            temp += "i";
        if constexpr (v == type<float>)
            temp += "f";
        if constexpr (v == type<double>)
            temp += "d";
        if constexpr (v == value<0>)
            temp += "0";
        if constexpr (v == value<1>)
            temp += "1";
        if constexpr (v == value<2>)
            temp += "2";
        if constexpr (v == value<3>)
            temp += "3";
    });

    if (temp == "i2d1f")
        return true;
    return false;
}

template<typename T, typename Q, typename v>
using apply_1 = Value<conjonction<is_same<T, int>, is_same<Q, float>, v() == 4>>;

// TODO
// static_assert(is_same<apply<apply_1, list<>>, error_type>);
static_assert(apply<apply_1, list<int, float, Value<4>>>::value);
static_assert(!apply<apply_1, list<char, float, Value<4>>>::value);
static_assert(!apply<apply_1, list<int, float, Value<5>>>::value);

static_assert(is_same<remove_doublons<list<>>, list<>>);
static_assert(is_same<remove_doublons<list<int>>, list<int>>);
static_assert(is_same<remove_doublons<list<int, int>>, list<int>>);
static_assert(is_same<remove_doublons<list<int, int, int>>, list<int>>);
static_assert(is_same<remove_doublons<list<int, float, int>>, list<int, float>>);
static_assert(is_same<remove_doublons<list<int, float, double, int, float, short>>,
                      list<int, float, double, short>>);

template<typename T> struct mult2_impl {};
template<auto v> struct mult2_impl<Value<v>> { using type = Value<2 * v>; };
template<typename T> using mult2 = typename mult2_impl<T>::type;

static_assert(is_same<map<mult2, list<>>, list<>>);
static_assert(is_same<map<mult2, integral_list<0, 1, 2, 3>>, integral_list<0, 2, 4, 6>>);
static_assert(is_same<map<size, list<list<int, short>>>, list<Value<2>>>);
static_assert(is_same<map<size, list<list<int, float, char>, list<short>, list<int, char, short, double>>>,
                      integral_list<3, 1, 4>>);

static_assert(is_same<get<0, list<>>, error_type>);
static_assert(is_same<get<3, list<short, int, float>>, error_type>);
static_assert(is_same<get<0, list<short>>, short>);
static_assert(is_same<get<0, list<short, int, float>>, short>);
static_assert(is_same<get<1, list<short, int, float>>, int>);
static_assert(is_same<get<2, list<short, int, float>>, float>);

static_assert(
      is_same<get<index_of<short, list<int, float, short, double>>, list<int, float, short, double>>, short>);

static_assert(is_same<permutation<list<>, integral_list<>>, list<>>);
static_assert(is_same<permutation<list<>, integral_list<0>>, list<error_type>>);
static_assert(is_same<permutation<list<short, int>, integral_list<>>, list<>>);
static_assert(is_same<permutation<list<short, int>, integral_list<0, 1>>, list<short, int>>);
static_assert(is_same<permutation<list<int, short>, integral_list<1, 0>>, list<short, int>>);
static_assert(
      is_same<permutation<list<short, int>, integral_list<2, 1, 0, 1>>, list<error_type, int, short, int>>);

template<typename T> using is_odd = Value<T::value % 2 == 1>;

static_assert(is_same<select<is_odd, list<>>, list<>>);
static_assert(is_same<select<is_odd, integral_list<0>>, list<>>);
static_assert(is_same<select<is_odd, integral_list<1>>, integral_list<1>>);
static_assert(is_same<select<is_odd, integral_list<0, 1, 2, 3, 5, 4, 6>>, integral_list<1, 3, 5>>);

static_assert(is_simple_permutation<list<>, list<>>);
static_assert(!is_simple_permutation<list<int>, list<>>);
static_assert(is_simple_permutation<list<int>, list<int>>);
static_assert(is_simple_permutation<list<int, float, int, double, char, char, short, char>,
                                    list<char, int, float, char, short, double, char, int>>);
static_assert(!is_simple_permutation<list<>, list<int>>);
static_assert(!is_simple_permutation<list<int>, list<>>);
static_assert(!is_simple_permutation<list<int>, list<float>>);
static_assert(!is_simple_permutation<list<float, float>, list<float>>);
static_assert(!is_simple_permutation<list<char>, list<char, char, char>>);
static_assert(!is_simple_permutation<list<int, char, float, char>, list<int, char, float>>);
static_assert(!is_simple_permutation<list<int, char, float, char>, list<float, int, char, float>>);

static_assert(is_same<intersect<list<>, list<>>, list<>>);
static_assert(is_same<intersect<list<>, list<int, float>>, list<>>);
static_assert(is_same<intersect<list<int, float>, list<>>, list<>>);
static_assert(is_same<intersect<list<int, float>, list<char, double>>, list<>>);
static_assert(is_same<intersect<list<int, float>, list<int, double>>, list<int>>);
static_assert(is_same<intersect<list<int, float>, list<short, int, double>>, list<int>>);
static_assert(is_same<intersect<integral_list<0, 1, 2, 4, 5, 8>, integral_list<2, 3, 5, 6, 7>>,
                      integral_list<2, 5>>);
static_assert(
      is_simple_permutation<intersect<integral_list<0, 2, 3, 2, 1, 3>, integral_list<1, 2, 4, 4, 4, 2, 3>>,
                            integral_list<1, 2, 2, 3>>);

static_assert(is_same<substract<list<>, list<>>, list<>>);
static_assert(is_same<substract<list<>, list<int, short>>, list<>>);
static_assert(is_same<substract<list<int, short>, list<>>, list<int, short>>);
static_assert(is_same<substract<list<int, short>, list<int>>, list<short>>);
static_assert(is_same<substract<list<int, short>, list<short>>, list<int>>);
static_assert(is_same<substract<list<int, short>, list<int, short>>, list<>>);
static_assert(
      is_same<substract<list<int, short, int, double, int>, list<int, int, short>>, list<double, int>>);

static_assert(is_same<zip<list<>, list<>>, list<>>);
static_assert(is_same<zip<list<>, list<int>>, error_type>);
static_assert(is_same<zip<list<int>, list<>>, error_type>);
static_assert(is_same<zip<list<int>, list<float>>, list<list<int, float>>>);
static_assert(
      is_same<zip<list<int, double>, list<float, char>>, list<list<int, float>, list<double, char>>>);
static_assert(is_same<zip<list<int>, list<float>, list<short>>, list<list<int, float, short>>>);
static_assert(
      is_same<zip<list<int, char, float>, list<double, short, long>, list<unsigned, long, char>>,
              list<list<int, double, unsigned>, list<char, short, long>, list<float, long, char>>>);

}  // namespace nlc::meta
#define TEST(f) res = (f) && res
int main() {
    auto res = true;
    TEST(nlc::meta::test_for_each_1());
    TEST(nlc::meta::test_for_each_2());
    return res ? 0 : 1;
}
