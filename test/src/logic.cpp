/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/meta/logic.hpp>
#include <nlc/meta/type_value.hpp>

using namespace nlc::meta;

// conjonction ///////////////////////////////////////////////////////////////

static_assert(conjonction<>);
static_assert(conjonction<true>);
static_assert(!conjonction<false>);
static_assert(conjonction<true, true>);
static_assert(!conjonction<true, false>);
static_assert(!conjonction<false, true>);
static_assert(!conjonction<false, false>);
static_assert(conjonction<true, true, true>);
static_assert(!conjonction<true, true, false>);
static_assert(!conjonction<true, false, true>);
static_assert(!conjonction<true, false, false>);
static_assert(!conjonction<false, true, true>);
static_assert(!conjonction<false, true, false>);
static_assert(!conjonction<false, false, true>);
static_assert(!conjonction<false, false, false>);

// disjonction ///////////////////////////////////////////////////////////////

static_assert(!disjonction<>);
static_assert(disjonction<true>);
static_assert(!disjonction<false>);
static_assert(disjonction<true, true>);
static_assert(disjonction<true, false>);
static_assert(disjonction<false, true>);
static_assert(!disjonction<false, false>);
static_assert(disjonction<true, true, true>);
static_assert(disjonction<true, true, false>);
static_assert(disjonction<true, false, true>);
static_assert(disjonction<true, false, false>);
static_assert(disjonction<false, true, true>);
static_assert(disjonction<false, true, false>);
static_assert(disjonction<false, false, true>);
static_assert(!disjonction<false, false, false>);

// negation //////////////////////////////////////////////////////////////////

static_assert(negation<false>);
static_assert(!negation<true>);

// if ////////////////////////////////////////////////////////////////////////

static_assert(type<if_t<true, int, float>> == type<int>);
static_assert(type<if_t<false, int, float>> == type<float>);

static_assert(type<if_default<int, float>> == type<int>);
static_assert(type<if_default<default_type, float>> == type<float>);

auto main() -> int {}
