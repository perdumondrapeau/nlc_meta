/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/meta/basics.hpp>
#include <nlc/meta/bit_cast.hpp>
#include <nlc/meta/is_same.hpp>
#include <nlc/meta/numeric_limits.hpp>
#include <nlc/meta/traits.hpp>

#include "../utils.hpp"

using namespace nlc::meta;

namespace impl {
template<typename T> struct integer_floating_point_limits final {};

template<> struct integer_floating_point_limits<float> final {
    static constexpr unsigned long long min               = 0b1'11111110'11111111111111111111111u;
    static constexpr unsigned long long max               = 0b0'11111110'11111111111111111111111u;
    static constexpr unsigned long long positive_infinity = 0b0'11111111'00000000000000000000000u;
    static constexpr unsigned long long negative_infinity = 0b1'11111111'00000000000000000000000u;
};

template<> struct integer_floating_point_limits<double> final {
    static constexpr unsigned long long min =
          0b1'11111111110'1111111111111111111111111111111111111111111111111111u;
    static constexpr unsigned long long max =
          0b0'11111111110'1111111111111111111111111111111111111111111111111111u;
    static constexpr unsigned long long positive_infinity =
          0b0'11111111111'0000000000000000000000000000000000000000000000000000u;
    static constexpr unsigned long long negative_infinity =
          0b1'11111111111'0000000000000000000000000000000000000000000000000000u;
};

template<typename T> struct same_size_signed_integer final {};
template<> struct same_size_signed_integer<float>    final { using type = signed int; };
template<> struct same_size_signed_integer<double>   final { using type = signed long long int; };
}  // namespace impl

template<typename T> auto test() {
    static_assert(is_same<T, rm_const_volatile<decltype(limits<T>::min)>>);
    static_assert(is_same<T, rm_const_volatile<decltype(limits<T>::max)>>);
}

template<typename T> auto test_integer() {
    test<T>();

    auto                  m  = limits<T>::min;
    auto                  M  = limits<T>::max;
    [[maybe_unused]] auto m2 = static_cast<T>(m - static_cast<T>(1));
    assert(m2 >= m);
    [[maybe_unused]] auto M2 = static_cast<T>(M + static_cast<T>(1));
    assert(M2 <= M);
}

template<typename T> auto test_floating_point() {
    using limits = limits<T>;

    test<T>();

    constexpr auto m  = limits::min;
    constexpr auto M  = limits::max;
    constexpr auto pi = limits::infinity;
    constexpr auto ni = -pi;

    static_assert(m < M);
    static_assert(M < pi);
    static_assert(m > ni);
    static_assert(pi * T { 3 } == pi);
    static_assert(pi * T { -3 } == ni);
    static_assert(ni * T { -3 } == pi);
    static_assert(ni * T { 3 } == ni);
    static_assert(limits::is_finite(m));
    static_assert(limits::is_finite(M));
    static_assert(!limits::is_finite(pi));
    static_assert(!limits::is_finite(ni));

    assert(!limits::is_nan(m));
    assert(!limits::is_nan(M));
    assert(!limits::is_nan(pi));
    assert(!limits::is_nan(ni));
    assert(limits::is_nan(pi * T { 0 }));

#if defined(__GNUC__) || defined(__clang__)
#    pragma GCC diagnostic push
#    if defined(__clang__)
#        pragma GCC diagnostic ignored "-Wunused-local-typedef"
#    elif defined(__GNUC__)
#        pragma GCC diagnostic ignored "-Wunused-local-typedefs"  // Damn GCC, for one letter ...
#    endif
#endif
    // No way to use [[maybe_unused]] on this :(
    using integer_limits = ::impl::integer_floating_point_limits<T>;
#if defined(__GNUC__) || defined(__clang__)
#    pragma GCC diagnostic pop
#endif

    [[maybe_unused]] auto const int_m  = bit_cast(m);
    [[maybe_unused]] auto const int_M  = bit_cast(M);
    [[maybe_unused]] auto const int_i  = bit_cast(pi);
    [[maybe_unused]] auto const int_ni = bit_cast(ni);

    assert(int_m == integer_limits::min);
    assert(int_M == integer_limits::max);
    assert(int_i == integer_limits::positive_infinity);
    assert(int_ni == integer_limits::negative_infinity);

    constexpr auto one = static_cast<T>(1);
    static_assert(one + limits::epsilon != one);

#if defined(__GNUC__) || defined(__clang__)
#    pragma GCC diagnostic push
#    if defined(__clang__)
#        pragma GCC diagnostic ignored "-Wundefined-reinterpret-cast"  // reinterpret_cast from
                                                                       // 'float_t' to 'int_t &'
                                                                       // has undefined behavior
#    elif defined(__GNUC__)
#        pragma GCC diagnostic ignored "-Wstrict-aliasing"  // dereferencing type-punned pointer
                                                            // will break strict-aliasing rules
#    endif
#endif
    using int_type = typename ::impl::same_size_signed_integer<T>::type;
    static_assert(sizeof(int_type) == sizeof(T));
    auto const epsilon_plus_one = one + limits::epsilon;
    // Compute next smallest float (1 less ULP)
    auto const smaller_epsilon_plus_one = reinterpret_cast<int_type const &>(epsilon_plus_one) - 1;
    // 1 + this value should be equal to 1
    assert(reinterpret_cast<T const &>(smaller_epsilon_plus_one) == one);
#if defined(__GNUC__) || defined(__clang__)
#    pragma GCC diagnostic pop
#endif
}

auto main() -> int {
    test_integer<char>();
    test_integer<signed char>();
    test_integer<unsigned char>();
    test_integer<short>();
    test_integer<unsigned short>();
    test_integer<int>();
    test_integer<unsigned int>();
    test_integer<long>();
    test_integer<unsigned long>();
    test_integer<long long>();
    test_integer<unsigned long long>();

    test_floating_point<float>();
    test_floating_point<double>();
}
