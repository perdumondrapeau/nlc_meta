/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/meta/basics.hpp>
#include <nlc/meta/is_same.hpp>
#include <nlc/meta/traits.hpp>

using namespace nlc::meta;

enum class Enum {};

struct Struct final {};

int main() { return 0; }

// is_integral ///////////////////////////////////////////////////////////////

static_assert(!is_integral<void>);
static_assert(!is_integral<Enum>);
static_assert(!is_integral<Struct>);
static_assert(is_integral<bool>);
static_assert(is_integral<char>);
static_assert(is_integral<signed char>);
static_assert(is_integral<unsigned char>);
static_assert(is_integral<char16_t>);
static_assert(is_integral<char32_t>);
static_assert(is_integral<wchar_t>);
static_assert(is_integral<short>);
static_assert(is_integral<unsigned short>);
static_assert(is_integral<int>);
static_assert(is_integral<unsigned int>);
static_assert(is_integral<long>);
static_assert(is_integral<unsigned long>);
static_assert(is_integral<long long>);
static_assert(is_integral<unsigned long long>);
static_assert(!is_integral<float>);
static_assert(!is_integral<double>);
static_assert(!is_integral<long double>);

// is_unsigned_integer ///////////////////////////////////////////////////////

static_assert(!is_unsigned_integer<void>);
static_assert(!is_unsigned_integer<Enum>);
static_assert(!is_unsigned_integer<Struct>);
static_assert(!is_unsigned_integer<bool>);
static_assert(!is_unsigned_integer<char>);
static_assert(!is_unsigned_integer<signed char>);
static_assert(is_unsigned_integer<unsigned char>);
static_assert(!is_unsigned_integer<char16_t>);
static_assert(!is_unsigned_integer<char32_t>);
static_assert(!is_unsigned_integer<wchar_t>);
static_assert(!is_unsigned_integer<short>);
static_assert(is_unsigned_integer<unsigned short>);
static_assert(!is_unsigned_integer<int>);
static_assert(is_unsigned_integer<unsigned int>);
static_assert(!is_unsigned_integer<long>);
static_assert(is_unsigned_integer<unsigned long>);
static_assert(!is_unsigned_integer<long long>);
static_assert(is_unsigned_integer<unsigned long long>);
static_assert(!is_unsigned_integer<float>);
static_assert(!is_unsigned_integer<double>);
static_assert(!is_unsigned_integer<long double>);

// is_signed_integer /////////////////////////////////////////////////////////

static_assert(!is_signed_integer<void>);
static_assert(!is_signed_integer<Enum>);
static_assert(!is_signed_integer<Struct>);
static_assert(!is_signed_integer<bool>);
static_assert(!is_signed_integer<char>);
static_assert(is_signed_integer<signed char>);
static_assert(!is_signed_integer<unsigned char>);
static_assert(!is_signed_integer<char16_t>);
static_assert(!is_signed_integer<char32_t>);
static_assert(!is_signed_integer<wchar_t>);
static_assert(is_signed_integer<short>);
static_assert(!is_signed_integer<unsigned short>);
static_assert(is_signed_integer<int>);
static_assert(!is_signed_integer<unsigned int>);
static_assert(is_signed_integer<long>);
static_assert(!is_signed_integer<unsigned long>);
static_assert(is_signed_integer<long long>);
static_assert(!is_signed_integer<unsigned long long>);
static_assert(!is_signed_integer<float>);
static_assert(!is_signed_integer<double>);
static_assert(!is_signed_integer<long double>);

// make_signed ///////////////////////////////////////////////////////////////

#define CHECK_MAKE_SIGNED_CV(TYPE, CV)                                        \
    static_assert(is_signed_integer<make_signed<TYPE CV>>);                   \
    static_assert(sizeof(make_signed<TYPE CV>) == sizeof(TYPE CV));           \
    static_assert(is_const<make_signed<TYPE CV>> == is_const<TYPE CV>);       \
    static_assert(is_volatile<make_signed<TYPE CV>> == is_volatile<TYPE CV>); \
    static_assert(is_const_volatile<make_signed<TYPE CV>> == is_const_volatile<TYPE CV>);

#define CHECK_MAKE_SIGNED(TYPE)          \
    CHECK_MAKE_SIGNED_CV(TYPE, )         \
    CHECK_MAKE_SIGNED_CV(TYPE, const)    \
    CHECK_MAKE_SIGNED_CV(TYPE, volatile) \
    CHECK_MAKE_SIGNED_CV(TYPE, const volatile)

#define CHECK_MAKE_SIGNED_FOR_TYPE(TYPE) \
    CHECK_MAKE_SIGNED(TYPE)              \
    CHECK_MAKE_SIGNED(signed TYPE)       \
    CHECK_MAKE_SIGNED(unsigned TYPE)

CHECK_MAKE_SIGNED_FOR_TYPE(char)
CHECK_MAKE_SIGNED_FOR_TYPE(short)
CHECK_MAKE_SIGNED_FOR_TYPE(int)
CHECK_MAKE_SIGNED_FOR_TYPE(long)
CHECK_MAKE_SIGNED_FOR_TYPE(long long)

#undef CHECK_MAKE_SIGNED_CV
#undef CHECK_MAKE_SIGNED
#undef CHECK_MAKE_SIGNED_FOR_TYPE

// make_unsigned /////////////////////////////////////////////////////////////

#define CHECK_MAKE_UNSIGNED_CV(TYPE, CV)                                        \
    static_assert(is_unsigned_integer<make_unsigned<TYPE CV>>);                 \
    static_assert(sizeof(make_unsigned<TYPE CV>) == sizeof(TYPE CV));           \
    static_assert(is_const<make_unsigned<TYPE CV>> == is_const<TYPE CV>);       \
    static_assert(is_volatile<make_unsigned<TYPE CV>> == is_volatile<TYPE CV>); \
    static_assert(is_const_volatile<make_unsigned<TYPE CV>> == is_const_volatile<TYPE CV>);

#define CHECK_MAKE_UNSIGNED(TYPE)          \
    CHECK_MAKE_UNSIGNED_CV(TYPE, )         \
    CHECK_MAKE_UNSIGNED_CV(TYPE, const)    \
    CHECK_MAKE_UNSIGNED_CV(TYPE, volatile) \
    CHECK_MAKE_UNSIGNED_CV(TYPE, const volatile)

#define CHECK_MAKE_UNSIGNED_FOR_TYPE(TYPE) \
    CHECK_MAKE_UNSIGNED(TYPE)              \
    CHECK_MAKE_UNSIGNED(signed TYPE)       \
    CHECK_MAKE_UNSIGNED(unsigned TYPE)

CHECK_MAKE_UNSIGNED_FOR_TYPE(char)
CHECK_MAKE_UNSIGNED_FOR_TYPE(short)
CHECK_MAKE_UNSIGNED_FOR_TYPE(int)
CHECK_MAKE_UNSIGNED_FOR_TYPE(long)
CHECK_MAKE_UNSIGNED_FOR_TYPE(long long)

#undef CHECK_MAKE_UNSIGNED_CV
#undef CHECK_MAKE_UNSIGNED
#undef CHECK_MAKE_UNSIGNED_FOR_TYPE

// is_integer ////////////////////////////////////////////////////////////////

static_assert(!is_integer<void>);
static_assert(!is_integer<Enum>);
static_assert(!is_integer<Struct>);
static_assert(!is_integer<bool>);
static_assert(!is_integer<char>);
static_assert(is_integer<signed char>);
static_assert(is_integer<unsigned char>);
static_assert(!is_integer<char16_t>);
static_assert(!is_integer<char32_t>);
static_assert(!is_integer<wchar_t>);
static_assert(is_integer<short>);
static_assert(is_integer<unsigned short>);
static_assert(is_integer<int>);
static_assert(is_integer<unsigned int>);
static_assert(is_integer<long>);
static_assert(is_integer<unsigned long>);
static_assert(is_integer<long long>);
static_assert(is_integer<unsigned long long>);
static_assert(!is_integer<float>);
static_assert(!is_integer<double>);
static_assert(!is_integer<long double>);

// is_floating ///////////////////////////////////////////////////////////////

static_assert(!is_floating<void>);
static_assert(!is_floating<Enum>);
static_assert(!is_floating<Struct>);
static_assert(!is_floating<bool>);
static_assert(!is_floating<char>);
static_assert(!is_floating<signed char>);
static_assert(!is_floating<unsigned char>);
static_assert(!is_floating<char16_t>);
static_assert(!is_floating<char32_t>);
static_assert(!is_floating<wchar_t>);
static_assert(!is_floating<short>);
static_assert(!is_floating<unsigned short>);
static_assert(!is_floating<int>);
static_assert(!is_floating<unsigned int>);
static_assert(!is_floating<long>);
static_assert(!is_floating<unsigned long>);
static_assert(!is_floating<long long>);
static_assert(!is_floating<unsigned long long>);
static_assert(is_floating<float>);
static_assert(is_floating<double>);
static_assert(is_floating<long double>);

// volatile //////////////////////////////////////////////////////////////////

static_assert(is_same<rm_volatile<int>, int>);
static_assert(is_same<rm_volatile<int const>, int const>);
static_assert(is_same<rm_volatile<int volatile>, int>);
static_assert(is_same<rm_volatile<int const volatile>, int const>);

static_assert(!is_volatile<int>);
static_assert(is_volatile<int volatile>);
static_assert(!is_volatile<int const>);
static_assert(is_volatile<int const volatile>);
static_assert(!is_volatile<int const &>);
static_assert(is_volatile<int volatile const &>);

static_assert(is_same<add_volatile<int>, int volatile>);
static_assert(is_same<add_volatile<int volatile>, int volatile>);

// reference /////////////////////////////////////////////////////////////////

static_assert(is_same<rm_ref<int>, int>);
static_assert(is_same<rm_ref<int &>, int>);
static_assert(is_same<rm_ref<int &&>, int>);
static_assert(is_same<rm_ref<int const>, int const>);
static_assert(is_same<rm_ref<int const &>, int const>);
static_assert(is_same<rm_ref<int const &&>, int const>);

static_assert(!is_lref<int>);
static_assert(is_lref<int &>);
static_assert(is_lref<int const &>);
static_assert(is_lref<int volatile &>);
static_assert(is_lref<int volatile const &>);
static_assert(!is_lref<int &&>);
static_assert(!is_rref<int>);
static_assert(!is_rref<int &>);
static_assert(is_rref<int &&>);
static_assert(is_rref<int const &&>);
static_assert(is_rref<int volatile &&>);
static_assert(is_rref<int volatile const &&>);

static_assert(is_same<add_lref<int>, int &>);
static_assert(is_same<add_lref<int &>, int &>);
static_assert(is_same<add_lref<int &&>, int &>);
static_assert(is_same<add_lref<int const>, int const &>);
static_assert(is_same<add_lref<int const &>, int const &>);
static_assert(is_same<add_lref<int const &&>, int const &>);
static_assert(is_same<add_rref<int>, int &&>);
static_assert(is_same<add_rref<int &>, int &>);
static_assert(is_same<add_rref<int &&>, int &&>);
static_assert(is_same<add_rref<int const>, int const &&>);
static_assert(is_same<add_rref<int const &>, int const &>);
static_assert(is_same<add_rref<int const &&>, int const &&>);

// const /////////////////////////////////////////////////////////////////////

static_assert(!is_const<int>);
static_assert(is_const<int const>);
static_assert(is_const<int const &>);
static_assert(!is_const<int &>);
static_assert(is_const<int volatile const>);
static_assert(!is_const<int volatile>);

static_assert(is_same<rm_const<int>, int>);
static_assert(is_same<rm_const<int const>, int>);

static_assert(is_same<add_const<int>, int const>);
static_assert(is_same<add_const<int const>, int const>);

// const ref and const volatile //////////////////////////////////////////////

static_assert(is_same<rm_const_ref<int>, int>);
static_assert(is_same<rm_const_ref<int &>, int>);
static_assert(is_same<rm_const_ref<int &&>, int>);
static_assert(is_same<rm_const_ref<int const>, int>);
static_assert(is_same<rm_const_ref<int const &>, int>);
static_assert(is_same<add_const_ref<int>, int const &>);
static_assert(is_same<add_const_ref<int &>, int const &>);
static_assert(is_same<add_const_ref<int &&>, int const &>);
static_assert(is_same<add_const_ref<int const>, int const &>);
static_assert(is_same<add_const_ref<int const &>, int const &>);

static_assert(is_same<rm_const_volatile<int>, int>);
static_assert(is_same<rm_const_volatile<int &>, int &>);
static_assert(is_same<rm_const_volatile<int const &>, int const &>);
static_assert(is_same<rm_const_volatile<int volatile>, int>);
static_assert(is_same<rm_const_volatile<int volatile &&>, int volatile &&>);
static_assert(is_same<rm_const_volatile<int const volatile>, int>);
static_assert(is_same<rm_const_volatile<int const volatile &>, int volatile const &>);
static_assert(is_same<rm_const_volatile<int const volatile &&>, int volatile const &&>);

// has_virtual_destructor ////////////////////////////////////////////////////

namespace v_descr {
struct A {};
struct B {
    virtual ~B() = default;
};
struct C : public A {};
struct D : public B {};
struct E : private A {};
struct F : private B {};

static_assert(!has_virtual_destructor<A>);
static_assert(has_virtual_destructor<B>);
static_assert(!has_virtual_destructor<C>);
static_assert(has_virtual_destructor<D>);
static_assert(!has_virtual_destructor<E>);
static_assert(has_virtual_destructor<F>);
}  // namespace v_descr

// is array //////////////////////////////////////////////////////////////////

namespace array {
static constexpr auto a   = "test";
static decltype(auto) b   = "test";
constexpr int         c[] = { 3, 5, 6 };
constexpr int *       d   = nullptr;

static_assert(!is_array<decltype(a)>);
static_assert(!is_array<decltype(b)>);
static_assert(is_array<rm_ref<decltype(b)>>);
static_assert(is_array<decltype(c)>);
static_assert(!is_array<decltype(d)>);

static_assert(array_len<rm_ref<decltype(b)>> == 5);
static_assert(array_len<decltype(c)> == 3);
}  // namespace array

// is empty type /////////////////////////////////////////////////////////////

namespace empty_type {
struct A {};
struct B {
    int a;
};

static_assert(is_empty_type<A>);
static_assert(!is_empty_type<B>);
}  // namespace empty_type

// is_polymorphic ////////////////////////////////////////////////////////////

namespace polymorphic {  // namespace polymorphic
// polymorphic type: declares a virtual member
struct Base1 {
    virtual ~Base1() {}
};

// polymorphic type: inherits a virtual member
struct Derived1 : Base1 {};

// non-polymorphic type
struct Base2 {};

// non-polymorphic type
struct Derived2 : Base2 {};

static_assert(is_polymorphic<Base1>);
static_assert(is_polymorphic<Derived1>);
static_assert(is_polymorphic<Base2> == false);
static_assert(is_polymorphic<Derived2> == false);
}  // namespace polymorphic

// has_unique_object_representations /////////////////////////////////////////

namespace unique_object_representations {
static_assert(has_unique_object_representations<char>);
static_assert(has_unique_object_representations<short>);
static_assert(has_unique_object_representations<int>);
static_assert(has_unique_object_representations<long int>);
static_assert(!has_unique_object_representations<float>);
static_assert(!has_unique_object_representations<double>);

struct Foo {
    int          a;
    unsigned int b;
    bool         c;
    char         d;
    short int    e;
};

struct Bar {
    int  a;
    char b;
};
static_assert(has_unique_object_representations<Foo>);
static_assert(!has_unique_object_representations<Bar>);
}  // namespace unique_object_representations

// is_constructible /////////////////////////////////////////

namespace constructible {
struct Foo final {
    Foo() = default;
};
struct Bar final {
    Bar(Foo const &) {}
    Bar(int) {}
    Bar(Foo, float, Foo &) {}
};

static_assert(is_constructible<Foo>);
static_assert(is_constructible<Foo, int> == false);
static_assert(is_constructible<Bar> == false);
static_assert(is_constructible<Bar, Foo>);
static_assert(is_constructible<Bar, Foo const &>);
static_assert(is_constructible<Bar, unsigned int>);
static_assert(is_constructible<Bar, Foo, float, Foo &>);
static_assert(is_constructible<Bar, Foo, float, Foo> == false);
}  // namespace constructible
