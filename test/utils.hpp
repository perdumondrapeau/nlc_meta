/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <stdlib.h>

#undef assert
#define assert(cond) \
    do {             \
        if (cond) {  \
        } else {     \
            exit(1); \
        }            \
    } while (false)

#if (defined(__GNUC__) || defined(__GNUG__)) && !defined(__clang__)
#    define static_assert_fails_to_compile(EXPR)                                                  \
        template<typename T> auto test_compilation_##__LINE__(float)->Value<true>;                \
        template<typename T> auto test_compilation_##__LINE__(int)->decltype(EXPR, value<false>); \
        static_assert(rm_const_ref<decltype(test_compilation_##__LINE__<int>(0))>::value);
#else
#    define static_assert_fails_to_compile(EXPR)
#endif
